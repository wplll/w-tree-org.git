// 功能插件
import log from '@/utils/log'
const EVENTS = {
  CLICK: 'on-node-click',
  DBLCLICK: 'on-node-dblclick',
  CONTEXTMENU: 'on-node-contextmenu',
  MOUSEENTER: 'on-node-mouseenter',
  MOUSELEAVE: 'on-node-mouseleave'
}

function createListener (handler, data) {
  if (typeof handler === 'function') {
    return function (e) {
      if (e.target.className.indexOf('org-tree-node-btn') > -1) return

      handler.apply(null, [e, data])
    }
  }
}
// 判断是否叶子节点
const isLeaf = (data, prop, ctx) => {
  // hack 根节点特殊处理
  if(ctx && Object.hasOwnProperty.call(data, 'showParents') && ctx.props.props.parents === prop){
    return !data.showParents
  }
  return !(Array.isArray(data[prop]) && data[prop].length > 0) && (data.isLeaf === undefined || data.isLeaf === true)
}

// 创建 node 节点
export const renderNode = (h, data, context, root, isParent = false) => {
  const { props } = context;
  const cls = [`tree-org${!root && isParent ? '-parent' : ''}-node`]
  const childNodes = []
  const children = data[props.props.children]

  if(root || !isParent) {
    if (isLeaf(data, props.props.children, context)) {
      cls.push('is-leaf')
    } else if (props.collapsable && !data[props.props.expand]) { // 追加是否展开class
      cls.push('collapsed')
    }
  }

  if(root || isParent) {
    // 表明有父节点
    if(isLeaf(data, props.props.parents, context)) {
      cls.push('is-parent-leaf')
    } else if(props.collapsable && !data[props.props['parent-expand']]) {
      cls.push('parent-collapsed')
    }
  }

  if(data.moving) {
    cls.push('tree-org-node__moving')
  }
  // 父级节点
  if(data.parents && (!props.collapsable || data[props.props['parent-expand']])){
    childNodes.push(renderParents(h, data.parents, context))
  }
  // 渲染label块
  childNodes.push(renderLabel(h, data, context, root, isParent))

  if (!props.collapsable || data[props.props.expand]) {
    childNodes.push(renderChildren(h, children, context))
  }
  log.print(childNodes)
  return h('div', {
    'class': cls,
    'key': data[props.props.id],
    'directives' :[ {
      name: 'show',
      value: !data.hidden
    }]
  }, childNodes)
}

// 创建展开折叠按钮
export const renderBtn = (h, data, context, isParent = false) => {
  const { props, listeners } = context;
  const expandHandler = listeners['on-expand']

  let cls = [`tree-org${isParent ? '-parent' : ''}-node__expand`]

  
  if(isParent && data[props.props['parent-expand']]){
    cls.push('parent-expanded')
  } 
  if (!isParent && data[props.props.expand]) {
    cls.push('expanded')
  }

  const children = []
  if (context.scopedSlots.expand) {
    children.push(context.scopedSlots.expand({node: data}))
  } else {
    children.push(h('span', {'class': 'tree-org-node__expand-btn'}))
  }
  return h('span', {
    'class': cls,
    on: {
      click: e => expandHandler && expandHandler(e, data, isParent)
    }
  }, children)
}

// 创建 label 节点
export const renderLabel = (h, data, context, root, isParent = false) => {
  const { props, listeners } = context
  const label = data[props.props.label]
  const renderContent = props.renderContent
  const { directives } = context.data;

  const childNodes = []

  const ifRenderParentBtn = root || isParent
  const ifRenderBtn = root || !isParent

  // 父节点 parent-expend
  if(ifRenderParentBtn && props.collapsable && !isLeaf(data, props.props.parents, context)) {
    childNodes.push(renderBtn(h, data, context, true))
  }

  if (context.scopedSlots.default) {
    childNodes.push(context.scopedSlots.default({node: data}))
  } else if (typeof renderContent === 'function') {
    log.warning('scoped-slot header is easier to use. We recommend users to use scoped-slot header.');
    let vnode = renderContent(h, data)
    vnode && childNodes.push(vnode)
  } else {
    childNodes.push(label)
  }

  if (ifRenderBtn && props.collapsable && !isLeaf(data, props.props.children, context)) {
    childNodes.push(renderBtn(h, data, context))
  }

  const cls = [`tree-org-node__inner`]
  let { labelStyle, labelClassName, selectedClassName, selectedKey } = props

  if (typeof labelClassName === 'function') {
    labelClassName = labelClassName(data)
  }

  labelClassName && cls.push(labelClassName)
  data.className && cls.push(data.className)
  // add selected class and key from props
  if (typeof selectedClassName === 'function') {
    selectedClassName = selectedClassName(data)
  }

  selectedClassName && selectedKey && data[selectedKey] && cls.push(selectedClassName)

  const nodeLabelClass = [`tree-org${!root && isParent ? '-parent' : ''}-node__content`]
  root && nodeLabelClass.push('tree-org-parent-node__content')
  if (root) {
    nodeLabelClass.push('is-root')
  } else if (data.newNode){
    nodeLabelClass.push('is-new')
  }
  // directives
  let cloneDirs 
  if(Array.isArray(directives)){
    cloneDirs = directives.map(item=>{
      const newValue = Object.assign({node: data}, item.value)
      return Object.assign({...item}, {value: newValue})
    })
  }
  // event handlers
  const NODEEVENTS = {};
  for (let EKEY in EVENTS){
    if (Object.prototype.hasOwnProperty.call(EVENTS,EKEY)){
      const EVENT = EVENTS[EKEY];
      let handler = listeners[EVENT];
      if(handler){
        NODEEVENTS[EKEY.toLowerCase()] = createListener(handler, data);
      }
    }
  }
  // texterea event handles
  const focusHandler = listeners['on-node-focus']
  const blurHandler = listeners['on-node-blur']
  return h('div', {
    'class': nodeLabelClass,
  }, [h('div', {
    'class': cls,
    'directives': root? [] : cloneDirs,
    style: data['style'] ? data['style'] : labelStyle,
    on: NODEEVENTS,
  },childNodes), h('textarea', {
    'class': `tree-org-node__textarea`,
    'directives' :[{
      name: 'show',
      value: data.focused
    }, {
      name: 'focus',
      value: data.focused
    }],
    "domProps":{
      placeholder: "请输入节点名称",
      value: data[props.props.label],
    },
    on: {
      focus: e => focusHandler && focusHandler(e, data),
      input: e => { data[props.props.label] = e.target.value },
      blur: e => { data.focused = false; blurHandler && blurHandler(e, data)},
      click: e => e.stopPropagation()
    }
  })])
}

// 创建 node 子节点
export const renderChildren = (h, list, context) => {
  if (Array.isArray(list) && list.length) {
    const children = list.map(item => {
      return renderNode(h, item, context, false)
    })

    return h('div', {
      'class': 'tree-org-node__children'
    }, children)
  }
  return ''
}

// 创建 父级node 节点
export const renderParents = (h, list, context) => {
  if(Array.isArray(list) && list.length){
    const parents = list.map(item => {
      return renderNode(h, item, context, false, true)
    })
    return h('div', {
      'class': 'tree-org-parent-node__children'
    }, parents)
  }
  return ''
}

export const render = (h, context) => {
  const { props } = context
  props.data.root = true;
  return renderNode(h, props.data, context, true, !isLeaf(props.data, props.props.parents, context))
}

export default render
