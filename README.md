# zm-tree-org

## 介绍
一个简易版组织架构图，组件依赖于vue-org-tree， 在此基础上将部分源代码进行优化修改。

### 安装
```
npm install w-tree-org --save
# or 
yarn add w-tree-org
```

### 引入
```
import Vue from 'vue';
import ZmTreeOrg from 'zm-tree-org';
import "zm-tree-org/lib/zm-tree-org.css";

Vue.use(ZmTreeOrg);
```


### 文档

[说明文档](https://sangtian152.gitee.io/zm-tree-org/).

